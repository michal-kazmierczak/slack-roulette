"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.slackRoulette = void 0;
const functions = require("firebase-functions");
const rp = require("request-promise");
const APP_CONFIG = functions.config();
exports.slackRoulette = functions
    .region('europe-west1')
    .pubsub
    .schedule('every day 12:37')
    .timeZone('Europe/Warsaw')
    .onRun(_ => {
    const luck = Math.round(Math.random() * 10000) / 10000;
    const probability = parseFloat(APP_CONFIG.app.probability);
    console.log('luck: ' + luck);
    if (luck <= probability) {
        return rp({
            method: 'POST',
            uri: APP_CONFIG.slack.webhook_url,
            body: {
                text: APP_CONFIG.slack.message_text,
                icon_emoji: APP_CONFIG.slack.message_icon_emoji,
                username: APP_CONFIG.slack.message_username
            },
            json: true
        }).promise();
    }
    return 'ok';
});
//# sourceMappingURL=index.js.map