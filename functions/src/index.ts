import * as functions from 'firebase-functions';
import * as rp from 'request-promise';

const APP_CONFIG = functions.config();

export const slackRoulette = functions
    .region('europe-west1')
    .pubsub
    .schedule('every day 00:00')
    .onRun(_ => {


    const luck = Math.round(Math.random() * 10_000) / 10_000;
    const probability = parseFloat(APP_CONFIG.app.probability);

    console.log('luck: ' + luck);
    if(luck <= probability) {
        return rp({
            method: 'POST',
            uri: APP_CONFIG.slack.webhook_url,
            body: {
                text: APP_CONFIG.slack.message_text,
                icon_emoji: APP_CONFIG.slack.message_icon_emoji,
                username: APP_CONFIG.slack.message_username
            },
            json: true
        }).promise();
    }

    return 'ok';
});
