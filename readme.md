# A pure rocket science 🚀

Post a Slack message[1] to a channel[2] at time[3] with proabability[4]

## Development
TODO

## Deployment
1. Setup a Firebase project
2. Provide `firebase.config()`
```bash
firebase functions:config:set \
  slack.webhook_url="https://hooks.slack.com/services/XXXX/XXXX/XXXX" \
  slack.message_text="HelloWorld!" \
  slack.message_icon_emoji=":rocket:" \
  slack.message_username="SlackRoulette" \
  app.probability="0.1234" \
  app.schedule_interval="every day 09:00" \
  app.schedule_timezone="Europe/Warsaw" \

$ firebase functions:config:get
{
  "slack": {
    "message_icon_emoji": ":rocket:",
    "webhook_url": "https://hooks.slack.com/services/XXXX/XXXX/XXXX",
    "message_username": "SlackRoulette",
    "message_text": "HelloWorld!"
  },
  "app": {
    "schedule_interval": "every day 09:00",
    "schedule_timezone": "Europe/Warsaw",
    "probability": "0.1234"
  }
}

```
